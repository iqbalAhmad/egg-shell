#include <sys/wait.h>   // waitpid()

#include <unistd.h>     //chdir(),fork(),exec(),pid_t

#include <stdlib.h>     //malloc(),realloc(),free(),exit(),execvp(),EXIT_SUCCESS, EXIT_FAILURE

#include <stdio.h>      //fprintf(),printf(),stderr,getchar(),perror()

#include <string.h>     // strcmp(),strtok()



#define BUFFER_SIZE 1024

#define TOKEN_BUFFER 64

#define TOKEN_DELIM " \t\n\r\a"



// Declaring some built in functions

int cd(char **args);

int help(char **args);

int shell_exit(char **args);

//---------------------------------

/* What shell do?

*It Read command

*separate them into token

*Run the parsed cmd

*/





//----------------PART 1:Reading input--------

char *read_line()

{

  int buffer_size = BUFFER_SIZE;

  int position = 0;

  char *buffer = malloc(sizeof(char) * buffer_size);

  int one_char;



  if (!buffer) {

    fprintf(stderr, "Memory allocation error\n");

    exit(EXIT_FAILURE);

  }



  while (1) {

    // Read a character

    one_char = getchar();



    // If we enter EOF, replace it with a null character and return.

    if (one_char == EOF || one_char == '\n') {

      buffer[position] = '\0';

      return buffer;

    } else {

      buffer[position] = one_char;

    }

    position++;



    // If we have exceeded the buffer, reallocate.

    if (position >= buffer_size) {

      buffer_size += BUFFER_SIZE;

      buffer = realloc(buffer, buffer_size);

      if (!buffer) {

        fprintf(stderr, "Memory reallocation error\n");

        exit(EXIT_FAILURE);

      }

    }

  }

}



//-------------PART 2:Split line-----



char **split_line(char *line)

{

  int buffer_size = TOKEN_BUFFER, position = 0;

  char **tokens = malloc(buffer_size * sizeof(char*));

  char *token;

  char s_token;



  if (!tokens) {

    fprintf(stderr, "Memory allocation for tokens error\n");

    exit(EXIT_FAILURE);

  }



  token = strtok(line, TOKEN_DELIM);   //line: which line, delimeter: where to stop



  while (token != NULL) {

    tokens[position] = token;

    position++;



    if (position >= buffer_size) {

      buffer_size += TOKEN_BUFFER;

      tokens = realloc(tokens, buffer_size * sizeof(char*));

      if (!tokens) {

        fprintf(stderr, "Memory reallocation for tokens error\n");

        exit(EXIT_FAILURE);

      }

    }



    token = strtok(NULL, TOKEN_DELIM);

  }

  tokens[position] = NULL;

  return tokens;

}



// ----------------------PART 3 : builtins and process exec----

//----------------process execution-------



int launching_program(char **args)

{

  pid_t Apid, waiting_pid; //process id type pid_t

  int status;



  Apid = fork();



  // Child process

  if (Apid == 0) {

    if (execvp(args[0], args) == -1) {

      perror("exec never return,error!!");

    }

    exit(EXIT_FAILURE); // exit from error situation

  }

  // Error forking

  else if (Apid < 0) {



    perror("fork return negative");

  }

  // Parent process

  else {



    do {

      waiting_pid = waitpid(Apid, &status, WUNTRACED); // parent process wait, her Apid is Child pid

    } while (!WIFEXITED(status) && !WIFSIGNALED(status));  //till process is not get killed or signaled to finish

  }// child process exited or not

   /*

    *WUNTRACED allows parent to be returned from waitpid() if a child gets stopped as well as exiting

    *or being killed. This way, parent has a chance to send it a SIGCONT to continue it, kill it,

    *assign its tasks to another child, whatever.

   */

  return 1;

}



//---------------Some built in functions for shell----



char *builtins[] = {"cd","help","exit"};



int (*builtin_func[])(char **) = {&cd,&help,&exit};  // Array of function address //Global



int total_builtins() {

  return sizeof(builtins) / sizeof(char *);

}



// Built in functions implementations

int cd(char **args)

{

  if (args[1] == NULL) {

    fprintf(stderr, "Expected argument to \"cd\"\n");

  } else {

    if (chdir(args[1]) != 0) {

      perror("error");

    }

  }

  return 1;

}



int help(char **args)

{

  int i;

  printf("Eggshell \n");

  printf("Type a program name and arguments, and hit enter.\n");

  printf("The following are built in:\n");



  for (i = 0; i < total_builtins(); i++) {

    printf("  %s\n", builtins[i]);

  } //print list of builtin services



    return 1;

}



int shell_exit(char **args)

{

  return 0;

}





int execute(char **splited_line)

{



  // For empty cmd

  if (splited_line[0] == NULL) {

    return 1;

  }

  // Builtins -----

  int i;

  for (i = 0; i < total_builtins(); i++) {

    if (strcmp(splited_line[0], builtins[i]) == 0) { // comapring for built in cmd

      return (*builtin_func[i])(splited_line);  // return the address of that function

    }

  }

  // Process executions---

  return launching_program(splited_line); // Or launch a program

}



void run (){







  char *line_ptr;

  char **splited_line;

  int status;



  do {

    printf("> ");

    line_ptr = read_line();

    splited_line = split_line(line_ptr);

    status = execute(splited_line);



    free(line_ptr);

    free(splited_line);

  } while (status);



}

//------------------MAIN--------------------

int main()

{

    run();



  return 0;

}
